const selectors = {
    usernameInput: '#user-name',
    passwordInput: '#password',
    loginButton: '#login-button'
}

class LoginPage {

    doLogin(data) {
        cy.log('Loggin into Swag labs...', data)
        this.enterUsername(data.username)
        this.enterPassword(data.password)
        this.clickLogin()
    }

    doLogout() {
        cy.log('Loggin off from Bittrap...')
        cy.get(selectors.logoutButton).click()
        cy.get(selectors.emailAddressInput).should('be.visible')
    }

    enterUsername(username) {
        cy.log(`Typing ${username}...`)
        cy.get(selectors.usernameInput)
            .type('{selectall}{backspace}' + username)
        return this
    }

    enterPassword(password) {
        cy.log('Typing password...')
        cy.get(selectors.passwordInput)
            .type('{selectall}{backspace}' + password)
        return this
    }

    clickLogin() {
        cy.log('Clicking on login button...')
        cy.get(selectors.loginButton).click()
        //cy.get(selectors.loggedPageSelector).should('be.visible')
    }
}

export const loginPageInstance = new LoginPage()
