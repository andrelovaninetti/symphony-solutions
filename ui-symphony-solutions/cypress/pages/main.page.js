const selectors = {
    shoppingCartIcon: '.shopping_cart_link',
    productSortDropDown: '.product_sort_container',
    activeOptionLbl: '.active_option',
    itemsName: '.inventory_item_name'
}

class MainPage {

    isShoppingCartIconVisible() {
        cy.log('Checking if login is DONE...')
        cy.get(selectors.shoppingCartIcon).should('be.visible')
    }

    isSortBy(criteria) {
        cy.log('Checking if sorted by following criteria: ' + criteria)
        cy.get(selectors.activeOptionLbl).contains(criteria)
    }

    /**
   * Given a column a desired order, it will select it.
   *
   * @param {string} order
   */
    selectColumnOrder(order) {
        cy.log(`Selecting order: ${order}`)
        cy.get(selectors.productSortDropDown).select(order)
    }

    verifySortOrder(order) {
        cy.get(selectors.itemsName).then($elements => {
            const text = [...$elements].map(el => el.innerText)
            this.verifyListOrder(order, text)
        })
    }

    verifyListOrder(order, text) {
        let copy = text.slice()
        let reversed = copy.sort().reverse()
        order === 'Name (A to Z)' ? expect(text).to.deep.equal(copy.sort())
            : expect(text).to.deep.equal(reversed)
    }

}

export const mainPageInstance = new MainPage()
