import { loginPageInstance } from '../pages/login.page'
import { mainPageInstance } from '../pages/main.page'

const credentials = {
  username: 'standard_user',
  password: 'secret_sauce'
}

const sortCriteria = {
  atoz: 'Name (A to Z)',
  ztoa: 'Name (Z to A)'
}

describe('Saucedemo', () => {

  before(() => {
    cy.visit('/')
  })

  it('Should login into the application', () => {
    cy.log('Loggin into Swag Labs...')
    loginPageInstance.doLogin(credentials)
    mainPageInstance.isShoppingCartIconVisible()    
    mainPageInstance.isSortBy(sortCriteria.atoz)

    mainPageInstance.selectColumnOrder(sortCriteria.ztoa)
    
    mainPageInstance.verifySortOrder(sortCriteria.ztoa)
  })
})