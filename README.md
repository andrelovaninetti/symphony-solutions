# API Symphony-solutions

## stack
- Docker
- Javascript
- npm  
- jest  
- superagent  
- eslint  
- allure  

This repository contains automated tests for a RESTfull api  
This is a basic and small framework just for challenge purposes. The same could be extended and improved as project requirements.  

## setup
1. Checkout repository  
```https://gitlab.com/andrelovaninetti/symphony-solutions.git```
2. Install **npm** if you don't have it installed
3. At project root level execute: ```npm install```
4. Run docker-compose build
5. Run docker-compose up -> All tests will be expected


# UI Symphony-solutions

## stack
- Docker
- Javascript
- npm  
- cypress

This repository contains automated tests for a UI
This is a basic and small framework just for challenge purposes. The same could be extended and improved as project requirements.  

## setup
1. Checkout repository  
```https://gitlab.com/andrelovaninetti/symphony-solutions.git```
2. Install **npm** if you don't have it installed
3. At project root level execute: ```npm install```
4. At project root level execute: ```npx cypress open```
