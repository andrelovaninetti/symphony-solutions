const api = require('../config/api.json')
const request = require('superagent')

const base_url = api.base_url
describe('Symphony', () => {

    const category = 'Category'
    const filter = 'Authentication & Authorization'

    it('should retrieve the data', async () => {
        const res = await request
            .get(base_url + api.service.entries)
        
        var filteredCollection = res.body.entries.filter((element) => {
            return element[category] === filter
        })

        console.log('Objects found: ' + filteredCollection.length)

        console.log(filteredCollection)
        
        // Assertions
        expect(res.status).toBe(200)
    })
    
})